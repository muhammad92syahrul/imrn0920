console.log("=====================================");
console.log("Jawaban IF ELSE");
console.log("=====================================");

var nama = "Syahrul";
var peran = "Werewolf";

if (nama.length<1)
{
    console.log("Nama harus diisi!");
}
else if (nama.length>1 && peran.length<1)
{
    console.log("Halo "+nama+", Pilih peranmu untuk memulai game!");
}else if (peran=="Penyihir"){
    console.log("Selamat datang di Dunia Werewolf, "+nama);
    console.log("Halo "+peran+" "+nama+", kamu dapat melihat siapa yang menjadi werewolf!");
}
else if (peran=="Guard"){
    console.log("Selamat datang di Dunia Werewolf, "+nama);
    console.log("Halo "+peran+" "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.");
}
else if (peran=="Werewolf"){
    console.log("Selamat datang di Dunia Werewolf, "+nama);
    console.log("Halo "+peran+" "+nama+", Kamu akan memakan mangsa setiap malam!");
}

console.log("=====================================");
console.log("Jawaban SWITCH CASE");
console.log("=====================================");

var hari = 21; 
var bulan = 1; 
var tahun = 1945;

var bulan_text;
switch(bulan)
{
    
    case 1:{
        bulan_text="Januari";
        break;
    }
    case 2:{
        bulan_text="Februari";
        break;
    }
    case 3:{
        bulan_text="Maret";
        break;
    }
    case 4:{
        bulan_text="April";
        break;
    }
    case 5:{
        bulan_text="Mei";
        break;
    }
    case 6:{
        bulan_text="Juni";
        break;
    }
    case 7:{
        bulan_text="Juli";
        break;
    }
    case 8:{
        bulan_text="Agustus";
        break;
    }
    case 9:{
        bulan_text="September";
        break;
    }
    case 10:{
        bulan_text="Oktober";
        break;
    }
    case 11:{
        bulan_text="November";
        break;
    }
    case 12:{
        bulan_text="Desember";
        break;
    }
    default:{
        bulan_text="";
        break;
    }

}

console.log(hari+" "+bulan_text+" "+tahun)