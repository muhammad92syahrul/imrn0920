console.log("=====================================");
console.log("1. Mengubah fungsi menjadi fungsi arrow");
console.log("=====================================");

golden = () => {
    console.log("this is golden!!")
}

golden()

console.log()
console.log()

console.log("=====================================");
console.log("2. Sederhanakan menjadi Object literal di ES6");
console.log("=====================================");

const newFunction = function literal(firstName, lastName) {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: () => console.log(firstName + " " + lastName)
    }
}

//Driver Code 
newFunction("William", "Imoh").fullName()

console.log()
console.log()

console.log("=====================================");
console.log("3. Destructuring");
console.log("=====================================");

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const { firstName, lastName, destination, occupation, spell } = newObject;

console.log(firstName, lastName, destination, occupation)

console.log()
console.log()

console.log("=====================================");
console.log("4. Array Spreading");
console.log("=====================================");

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combinedArray = [...west, ...east]
//Driver Code
console.log(combinedArray)


console.log()
console.log()

console.log("=====================================");
console.log("5. Template Literals");
console.log("=====================================");

const planet = "earth"
const view = "glass"
const after = `Lorem ${view} dolor sit amet, 
    consectetur adipiscing elit, ${planet} do eiusmod tempor 
    incididunt ut labore et dolore magna aliqua. Ut enim
    ad minim veniam`

// Driver Code
console.log(after) 