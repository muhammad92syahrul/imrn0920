
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class SkillScreen extends Component {

    render() {
        let video = this.props.video;
        return (
            <View style={styles.container}>
                <Image source={{ uri: video.snippet.thumbnails.medium.url }} style={{ height: 200 }} />
                <View style={styles.descContiner}>
                    <Image source={{ uri: 'https://randomuser.me/api/portraits/men/0.jpg' }} style={{ width: 50, height: 50, borderRadius: 25 }} />
                    <View style={styles.videoDetails}>
                        <Text style={styles.videoTitle}>{video.snippet.title}</Text>
                        <Text style={styles.videoStats}>{video.snippet.channelTitle + " . " + video.statistics.viewCount + " . 3 months ago"}</Text>
                    </View>
                </View>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        padding: 15,
    },
    descContiner: {
        flexDirection: 'row',
        paddingTop: 15
    },
    videoDetails: {
        paddingHorizontal: 15,
        flex: 1
    },
    videoTitle: {
        fontSize: 16,
        color: "#3c3c3c"
    },
    videoStat: {
        fontSize: 14,
        paddingTop: 3
    }

});