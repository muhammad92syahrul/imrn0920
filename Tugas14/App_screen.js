import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, FlatList } from 'react-native';
// import Icon from 'react-native-vector-icons/MaterialIcons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
// import App1 from './Tugas/Tugas12/VideoItem';
// import VideoItem from './Tugas/Tugas12/components/videoItem';
// import data from './Tugas/Tugas12/data.json';

export default function App() {
    return (
        // <App1 />
        <View style={styles.container}>
            <View style={styles.navBar}>
                <Image source={require('./logo.png')} style={{ width: 225, height: 75 }} />
                <Text style={styles.porto}>PORTOFOLIO</Text>
            </View>
            <View style={styles.hi}>
                <MaterialCommunityIcons name="account-circle" size={45} color="#3EC6FF" />
                <View style={styles.hibagi}>
                    <Text style={styles.hiTitle}>Hai,</Text>
                    <Text style={styles.hiTitle}>Muhammad Syahrul</Text>
                </View>
            </View>
            <View style={styles.skill}>
                <Text style={styles.skillTitle}>SKILL</Text>
                <View style={styles.kotakPanjang}></View>
            </View>
            <View style={styles.body}>
                {/* <VideoItem video={data.items[0]} /> */}
                {/* <FlatList data={data.items}
                    renderItem={(video) => <VideoItem video={video.item} />}
                /> */}


            </View>

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingLeft: 23,
        paddingRight: 23,
    },
    porto: {
        color: '#3EC6FF',
        fontSize: 12,
        lineHeight: 14,

    },
    hi: {
        flexDirection: 'row',
        paddingTop: 15
    },
    hibagi: {
        flexDirection: 'column',
        justifyContent: 'space-around',
        paddingTop: 3,
        marginLeft: 5,
    },
    hiTitle: {
        color: '#000000',
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 12,
        lineHeight: 14,
    },
    skill: {
        paddingTop: 10,
        flexDirection: 'column'
    },
    skillTitle: {
        color: '#003366',
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 36,
        lineHeight: 42,
    },
    kotakPanjang: {

        width: 343,
        height: 4,
        // left: 16px;
        // top: 145px;

        backgroundColor: '#3EC6FF',
    },
    navBar: {
        // flex: 2,
        height: 100,
        backgroundColor: 'white',
        elevation: 3,
        marginTop: 20,
        paddingHorizontal: 15,
        flexDirection: 'column',
        alignItems: 'flex-end',
        justifyContent: 'flex-start'
    },
    rightNav: {
        flexDirection: 'row',
    },
    navItem: {
        marginLeft: 25
    },
    body: {
        flex: 3,
    },
    tabBar: {
        backgroundColor: 'white',
        height: 60,
        borderTopWidth: 0.5,
        borderColor: '#E5E5E5',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    tabItem: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    tabTitle: {
        fontSize: 11,
        color: '#3c3c3c',
        paddingTop: 4

    }

});
