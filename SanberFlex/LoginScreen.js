import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

export default function LoginScreen() {
    return (
        <View style={styles.container}>
            <Image source={require('./Tugas13/images/logo.png')} style={styles.logo_sanber} />

            <Text style={styles.portofolio}>PORTOFOLIO</Text>
            <View style={styles.divLogin}>
                <Text style={styles.titleLogin}>Login</Text>
            </View>

            <Text style={styles.label1}>Username/Email</Text>
            <View style={styles.kotak1}></View>
            <Text style={styles.label2}>Password</Text>
            <View style={styles.kotak2}></View>
            <View style={styles.rec}></View>
            <Text style={styles.recTitle}>Masuk</Text>

            <Text style={styles.atauTitle}>atau</Text>
            <View style={styles.recDaftar}></View>
            <Text style={styles.daftarTitle}>Daftar?</Text>

        </View >
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    logo_sanber: {
        height: 102,
        width: 375,
        left: 0,
        top: 63,
        position: 'absolute'
    },
    portofolio: {
        height: 28,
        width: 142,
        left: 200,
        top: 143,
        fontSize: 20,
        fontStyle: 'normal',
        fontFamily: 'Roboto',
        fontWeight: 'normal',
        lineHeight: 28,
        color: '#3EC6FF',
        position: 'absolute'

    },
    titleLogin: {
        position: 'absolute',
        width: 60,
        height: 34,
        left: 158,
        top: 235,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 21,
        lineHeight: 28,
        color: '#003366'

    },
    divLogin: {
        flex: 1,
        alignItems: 'center'
    },
    label1: {
        position: 'absolute',
        width: 'auto',
        height: 19,
        left: 41,
        top: 303,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16,
        lineHeight: 19,
        color: '#003366'
    },
    label2: {
        position: 'absolute',
        width: 'auto',
        height: 19,
        left: 41,
        top: 390,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16,
        lineHeight: 19,
        color: '#003366'
    },
    kotak1: {
        position: 'absolute',
        width: 294,
        height: 48,
        left: 41,
        top: 326,

        backgroundColor: 'rgb(255, 255, 255)',
        borderColor: '#003366',
        borderBottomWidth: 1,
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderTopWidth: 1,
    },
    kotak2: {
        position: 'absolute',
        width: 294,
        height: 48,
        left: 41,
        top: 413,

        backgroundColor: 'rgb(255, 255, 255)',
        borderColor: '#003366',
        borderBottomWidth: 1,
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderTopWidth: 1,
    },
    rec: {
        position: 'absolute',
        width: 140,
        height: 40,
        left: 118,
        top: 493,

        backgroundColor: '#3EC6FF',
        zIndex: 1,
        borderRadius: 16,
        // border
        // border-radius: 16px;
    },
    recTitle: {
        position: 'absolute',
        width: 'auto',
        height: 28,
        left: 152,
        top: 499,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 24,
        lineHeight: 28,
        color: '#FFFFFF',
        zIndex: 10,

    },
    atauTitle: {
        position: 'absolute',
        width: 'auto',
        height: 28,
        left: 164,
        top: 549,

        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 24,
        lineHeight: 28,

        color: '#3EC6FF',
    },
    daftarTitle: {
        position: 'absolute',
        width: 'auto',
        height: 28,
        left: 146,
        top: 599,

        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 24,
        lineHeight: 28,

        color: '#FFFFFF',

    },
    recDaftar: {
        position: 'absolute',
        width: 140,
        height: 40,
        left: 118,
        top: 593,

        backgroundColor: '#003366',
        borderRadius: 16,
        // border-radius: 16px;
    }
});
