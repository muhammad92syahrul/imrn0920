import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default function AboutScreen() {
    return (
        <View style={styles.container}>
            <Text style={styles.tentang}>Tentang Saya</Text>
            <View style={styles.bundar}></View>

            <Text style={styles.nama}>Nama Lengkap</Text>
            <Text style={styles.subnama}>React Native Developer</Text>
            <View style={styles.kotak1}></View>

            <Text style={styles.portofolio}>Portofolio</Text>
            <View style={styles.line}></View>
            <Text style={styles.gitTitle}>akun gitlab</Text>
            <Text style={styles.hubTitle}>akun github</Text>

            <View style={styles.kotak2}></View>
            <Text style={styles.hubungi}>Hubungi Saya</Text>
            <View style={styles.line2}></View>
            {/* <Icon style={styles.iconfb} name="search" size={25} /> */}
            <Text style={styles.fbTitle}>akun fb</Text>
            <Text style={styles.igTitle}>akun ig</Text>
            <Text style={styles.twitterTitle}>akun twitter</Text>


        </View >
    );
}

const styles = StyleSheet.create({
    tentang: {
        position: 'absolute',
        width: 'auto',
        height: 'auto',
        left: 78,
        top: 64,

        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 36,
        lineHeight: 42,

        color: '#003366',
    },
    bundar: {
        position: 'absolute',
        width: 200,
        height: 200,
        left: 88,
        top: 118,

        backgroundColor: '#EFEFEF',
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
    },

    nama: {
        height: 'auto',
        width: 'auto',
        left: 106,
        top: 342,
        fontSize: 24,
        fontStyle: 'normal',
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        lineHeight: 28,
        color: '#003366',
        position: 'absolute'

    },
    subnama: {
        position: 'absolute',
        width: 'auto',
        height: 'auto',
        left: 104,
        top: 378,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 16,
        lineHeight: 19,
        color: '#3EC6FF'

    },

    portofolio: {
        position: 'absolute',
        width: 'auto',
        height: 21,
        left: 33,
        top: 418,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 18,
        lineHeight: 21,
        color: '#003366'
    },
    label2: {
        position: 'absolute',
        width: 'auto',
        height: 19,
        left: 41,
        top: 390,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 16,
        lineHeight: 19,
        color: '#003366'
    },
    kotak1: {
        position: 'absolute',
        width: 359,
        height: 140,
        left: 30,
        top: 413,

        backgroundColor: '#EFEFEF',
        borderRadius: 16,

    },
    kotak2: {
        position: 'absolute',
        width: 359,
        height: 240,
        left: 30,
        top: 562,

        backgroundColor: '#EFEFEF',
        borderRadius: 16,
    },
    line: {
        position: 'absolute',
        width: 343,
        height: 0,
        left: 31,
        top: 447,
        borderBottomWidth: 1,
    },
    line2: {
        position: 'absolute',
        width: 343,
        height: 0,
        left: 31,
        top: 598,
        borderBottomWidth: 1,

    },
    hubungi: {
        position: 'absolute',
        width: 'auto',
        height: 21,
        left: 33,
        top: 569,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: 18,
        lineHeight: 21,
        color: '#003366'
    },
    gitTitle: {
        position: 'absolute',
        width: 'auto',
        height: 'auto',
        left: 53,
        top: 517,

        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 16,
        lineHeight: 19,

        color: '#003366',

    },
    hubTitle: {
        position: 'absolute',
        width: 'auto',
        height: 'auto',
        left: 241,
        top: 517,

        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 16,
        lineHeight: 19,

        color: '#003366',

    },
    fbTitle: {
        position: 'absolute',
        width: 'auto',
        height: 'auto',
        left: 156,
        top: 627,

        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 16,
        lineHeight: 19,

        color: '#003366',

    },
    igTitle: {
        position: 'absolute',
        width: 'auto',
        height: 'auto',
        left: 156,
        top: 696,

        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 16,
        lineHeight: 19,

        color: '#003366',

    },
    twitterTitle: {
        position: 'absolute',
        width: 'auto',
        height: 'auto',
        left: 156,
        top: 765,

        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 16,
        lineHeight: 19,

        color: '#003366',

    },

});
