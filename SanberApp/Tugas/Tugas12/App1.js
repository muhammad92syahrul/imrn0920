import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';


export default function App1() {
    return (
        <View style={styles.container}>
            <Text>Open up App.js to start working on sip app!</Text>
            <StatusBar style="auto" />,
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
