console.log("=====================================");
console.log("No. 1 Looping While");
console.log("=====================================");

var N=1
console.log("LOOPING PERTAMA");
while(N<=20){
    if(N % 2==0){
        console.log(N+" - I Love Coding");
    }
    // console.log(N);
    N=N+1;
}
console.log("LOOPING KEDUA");
while(N>=2)
{
    if(N % 2==0){
        console.log(N+" - I will become a mobile developer");
        
    }
    N=N-1;
}

console.log("=====================================");
console.log("No. 2 Looping menggunakan for");
console.log("=====================================");

M=1

for(M=1;M<=20;M++)
{
    if(M%2==1)
    {
        if(M%3==0)
        {
            console.log(M+" - I Love Coding");
        }else{
            console.log(M+" - santai");
        }
        
    }
    else 
    {
        console.log(M+" - berkualitas");
    }
}

console.log("=====================================");
console.log("No. 3 Membuat Persegi Panjang #");
console.log("=====================================");

for(i=1;i<=4;i++)
{
    for(j=1;j<=8;j++)
    {
        process.stdout.write("#")
    }
    console.log()
}

console.log("=====================================");
console.log("No. 4 Membuat Tangga");
console.log("=====================================");

for(i=1;i<=7;i++)
{
    for(j=1;j<=7;j++)
    {
        if(i>=j)
        {
            process.stdout.write("#")
        }
    }
    console.log()
}

console.log("=====================================");
console.log("No. 5 Membuat Papan Catur");
console.log("=====================================");

for(i=1;i<=8;i++)
{
    for(j=1;j<=8;j++)
    {
        if((j%2==0 && i%2==1) || (j%2==1 && i%2==0))
        {
            process.stdout.write("#")
        }else{
            process.stdout.write(" ")
        }
    }
    console.log()
}