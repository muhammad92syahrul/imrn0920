console.log("=====================================");
console.log("Soal No. 1 (Range)");
console.log("=====================================");

function range(startNum, finishNum) {
    var a = [];
    var b = startNum;
    if (startNum >= finishNum) {

        while (b >= finishNum) {
            a.push(b);
            b -= 1;
        }
    } else if (startNum < finishNum) {
        while (b <= finishNum) {
            a.push(b);
            b += 1;
        }
    } else {
        a.push(-1);
    }
    return a;
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log("=====================================");
console.log("Soal No. 2 (Range with Step)");
console.log("=====================================");

function rangeWithStep(startNum, finishNum, step = 1) {
    var a = [];
    var b = startNum;
    if (startNum >= finishNum) {

        while (b >= finishNum) {
            a.push(b);
            b -= step;
        }
    } else if (startNum < finishNum) {
        while (b <= finishNum) {
            a.push(b);
            b += step;
        }
    } else {
        a.push(-1);
    }
    return a;
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log("=====================================");
console.log("Soal No. 3 (Sum of Range)");
console.log("=====================================");

function sum(startNum, finishNum, step = 1) {
    if (typeof finishNum == 'undefined') {
        if (typeof startNum !== 'undefined') {
            return startNum;
        } else {
            return 0;
        }
    } else {
        var a = rangeWithStep(startNum, finishNum, step);
        var sum = 0;
        for (i = 0; i < a.length; i++) {
            sum += a[i];
        }
    }
    return sum;
}



console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 


console.log("=====================================");
console.log("Soal No. 4 (Array Multidimensi)");
console.log("=====================================");


function dataHandling(data) {
    var txt = "";
    for (n in data) {
        txt += "Nomor ID:  " + input[n][0] + "\nNama Lengkap:  " + input[n][1] + "\nTTL:  " + input[n][2] + " " + input[n][3] + "\nHobi:  " + input[n][4] + "\n\n";
        //console.log(input[n][0]);
    }
    return txt;
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

console.log(dataHandling(input))


console.log("=====================================");
console.log("Soal No. 5 (Balik Kata)");
console.log("=====================================");

function balikKata(kata) {
    var l = kata.length;
    var N = 0;
    var a = "";
    while (N < l) {
        a = a + kata[l - N - 1];
        N = N + 1;
    }
    return a;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 


console.log("=====================================");
console.log("Soal No. 6 (Metode Array)");
console.log("=====================================");

function dataHandling2(ar) {
    var txt = "";
    var edar = ar.slice(0, 5);

    edar.pop();
    edar.splice(4, 0, "Pria", "SMA Internasional Metro");
    edar[1] = "Roman Alamsyah Elsharawy";
    edar[2] = "Provinsi Bandar Lampung";

    txt += '["' + edar[0] + '","' + edar[1] + '","' + edar[2] + '","' + edar[3] + '","' + edar[4] + '","' + edar[5] + '"]\n';

    var tgl = ar[3].split("/");
    var bln;
    switch (tgl[1].toString()) {
        case "01": {
            bln = "Januari";
            break;
        }
        case "02": {
            bln = "Februari";
            break;
        }
        case "03": {
            bln = "Maret";
            break;
        }
        case "04": {
            bln = "April";
            break;
        }
        case "05": {
            bln = "Mei";
            break;
        }
        case "06": {
            bln = "Juni";
            break;
        }
        case "07": {
            bln = "Juli";
            break;
        }
        case "08": {
            bln = "Agustus";
            break;
        }
        case "09": {
            bln = "September";
            break;
        }
        case "10": {
            bln = "Oktober";
            break;
        }
        case "11": {
            bln = "November";
            break;
        }
        case "12": {
            bln = "Desember";
            break;
        }
        default: {
            bln = "";
            break;
        }
    }

    txt += bln.toString() + "\n";
    txt += '["' + tgl[2] + '","' + tgl[0] + '","' + tgl[1] + '"]\n';
    txt += tgl.join("-") + '\n';
    txt += ar[1].toString() + "\n";

    return txt;

}

var input2 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
console.log(dataHandling2(input2));
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */