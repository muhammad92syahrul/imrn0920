console.log("=====================================");
console.log("Soal No. 1 (Array to Object)");
console.log("=====================================");

function arrayToObject(arr) {

    var text = "";
    for (i = 0; i < arr.length; i++) {
        var now = new Date()
        var thisYear = now.getFullYear() // 2020 (tahun sekarang)
        var umur;
        if (arr[i][3] != "undefined" && thisYear > arr[i][3]) {
            umur = thisYear - arr[i][3]
        } else {
            umur = "Invalid Birth Year";
        }
        var obj =
        {
            firstName: arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2],
            age: umur,
        }
        var myJSON = JSON.stringify(obj);
        text += (i + 1) + ". " + obj.firstName + " " + obj.lastName + " : " + myJSON + "\n";
    }
    return text;
}

var input = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
console.log(arrayToObject(input))


console.log("=====================================");
console.log("Soal No. 2 (Shopping Time)");
console.log("=====================================");

function shoppingTime(memberId, money) {
    var txt = "";
    var arr_barang = [1500000, 500000, 250000, 175000, 50000];
    // var urut_barang = arr_barang.sort(function (a, b) { return b - a })
    var arr_label = ['Sepatu Stacattu', 'Baju Zoro', 'Baju H&N', 'Sweater Uniklooh', 'Casing Handphone'];

    if (memberId.length < 1) {
        txt = "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if (money < 50000) {
        txt = "Mohon maaf, uang tidak cukup"
    } else {
        var sisa = money
        var beli = []

        for (var i = 0; i < arr_barang.length; i++) {
            if (sisa >= arr_barang[i] && !beli.includes(arr_label[i])) {
                beli.push(arr_label[i])
                sisa = sisa - arr_barang[i];
            }
        }

        var txt = {
            memberId: memberId,
            money: money,
            listPurchased: beli,
            changeMoney: sisa,
        }
    }
    return txt;
    // you can only write your code here!
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));

console.log("=====================================");
console.log("Soal No. 3 (Naik Angkot)");
console.log("=====================================");

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var satuan = 2000;
    var simpan = [];
    //your code here
    for (i = 0; i < arrPenumpang.length; i++) {
        var b = rute.indexOf(arrPenumpang[i][2]);
        var a = rute.indexOf(arrPenumpang[i][1]);
        var biaya;
        var selisih = b - a;

        if (selisih > 0) {
            biaya = satuan * selisih;
        } else {
            biaya = 0;
        }

        var obj = {
            penumpang: arrPenumpang[i][0],
            naikDari: arrPenumpang[i][1],
            tujuan: arrPenumpang[i][2],
            bayar: biaya,
        }
        simpan.push(obj)
    }
    return simpan;
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));