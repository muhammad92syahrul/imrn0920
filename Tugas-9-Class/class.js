console.log("=====================================");
console.log("1. Animal Class ");
console.log("=====================================");

class Animal {
    constructor(name) {
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }
}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false



console.log();

class Ape extends Animal {
    constructor(name) {
        super(name);
        this.legs = 2;
    }
    yell() {
        console.log("Auooo");
    }
}

class Frog extends Animal {
    constructor(name) {
        super(name);
    }
    jump() {
        console.log("hop hop");
    }
}

var sungokong = new Ape("kera sakti")
// console.log(sungokong)
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
// console.log(kodok)
kodok.jump() // "hop hop" 


console.log("=====================================");
console.log("2. Function to Class");
console.log("=====================================");
function Clock2({ template }) {

    var timer;

    function render() {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }

    this.stop = function () {
        clearInterval(timer);
    };

    this.start = function () {
        render();
        timer = setInterval(render, 1000);
    };

}



class Clock {
    constructor(template) {
        this._template = 'h:m:s';
        this._timer;
    }

    start() {
        this._timer = setInterval(
            this.render.bind(this),
            1000
        )
    }

    stop() {
        clearInterval(this._timer);
    }

    render() {

        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this._template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }
}

var clock = new Clock('h:m:s');
clock.start();

